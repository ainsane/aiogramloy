

import asyncio
import logging
from aiohttp import web
from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.dispatcher.webhook import get_new_configured_app
from aiogram.types import ContentTypes


import loggs
import mongoworker
import loyAPI
import keyboards
import pagination

TOKEN = '671483817:AAHJI-Nluxqi87VyKYGhufSpmLh73SLyH1U'

WEBHOOK_HOST = 'https://1ce83671.ngrok.io'
WEBHOOK_URL_PATH = '/'

WEBHOOK_URL = f"https://{WEBHOOK_HOST}{WEBHOOK_URL_PATH}"

WEBAPP_HOST = 'localhost'
WEBAPP_PORT = 5000

loop = asyncio.get_event_loop()
bot = Bot(TOKEN, loop=loop)
dp = Dispatcher(bot)

mongo = mongoworker.MongoWorker()
loyapi = loyAPI.LoyAPI(loop)

logging.basicConfig(level=logging.INFO)
erlog = loggs.errorlog
infolog = loggs.infolog

pag = pagination.Pagination()
pagnews = pagination.PaginationNewsActions()
pagactions = pagination.PaginationNewsActions()


@dp.errors_handler()
async def error(update, exception):
    erlog.error(f'Something goes wrong {exception}')
    return True


@dp.message_handler(commands=['start'])
async def start_comm(message):
    try:
        user = await mongo.find_user(str(message.chat.id))
        if 'token' in user:
            await bot.send_message(str(message.chat.id),
                                   f'Приветствую, {message.chat.first_name}!\nДобро пожаловать в ABM Loyalty!!!',
                                   reply_markup=keyboards.menu())
            await mongo.set_user_state(str(message.chat.id), 'ready')

        elif 'phone' in user:
            try:
                phone_numb = user['phone']
                need_passwd = await loyapi.ifneedpass()
                if need_passwd is True:
                    if user['state'] == 'passregister':
                        await bot.send_message(message.chat.id,
                                               'Придумай и отправь мне пароль:',
                                               reply_markup=keyboards.ReplyKeyboardRemove())
                    elif user['state'] == 'passauth':
                        await bot.send_message(message.chat.id,
                                               'Введи свой пароль',
                                               reply_markup=keyboards.ReplyKeyboardRemove())
                else:
                    auth_phone = await loyapi.auth_phone(phone_numb)
                    if auth_phone == 'notreg':
                        await bot.send_message(message.chat.id,
                                               'Номер не зарегестрирован в системе',
                                               reply_markup=keyboards.ReplyKeyboardRemove())
                        await bot.send_message(message.chat.id,
                                               'Пройти регистрацию:',
                                               reply_markup=keyboards.register())
                    elif auth_phone[0] == 'ok':
                        await mongo.set_user_state(str(message.chat.id), {'auth': auth_phone[1]})
                        await bot.send_message(message.chat.id,
                                               'Мы переотправили SMS\nВведите код из SMS сообщения',
                                               reply_markup=keyboards.ReplyKeyboardRemove())

            except Exception as e:
                erlog.exception(e)

        elif user['state'] == 'nophone':
            await bot.send_message(message.chat.id,
                                   "Привет, Добро пожаловать в ABM Loyalty!!! "
                                   "\nДля начала работы, поделись номером телефона📱 "
                                   "\n (псс...👀 Я тебя запомню...)",
                                   reply_markup=keyboards.phonenum())

    except TypeError:
        await bot.send_message(message.chat.id,
                               "Привет, Добро пожаловать в ABM Loyalty!!! "
                               "\nДля начала работы, поделись номером телефона📱 "
                               "\n (псс...👀 Я тебя запомню...)",
                               reply_markup=keyboards.phonenum())
        await mongo.write_user(str(message.chat.id), str(message.from_user.first_name), 'nophone')

    except Exception as e:
        erlog.exception(e)


@dp.message_handler(content_types=['contact'])
async def contact(message):
    try:
        user = await mongo.find_user(str(message.chat.id))
        need_pass = await loyapi.ifneedpass()
        if need_pass is True and user['state'] == 'nophone' and message.from_user.id == message.contact.user_id:
            try:
                phone_numb = ''.join(filter(lambda x: x.isdigit(), str(message.contact.phone_number)))
                await mongo.set_user_phone(str(message.chat.id), str(phone_numb), 'phone')

                auth_phone = await loyapi.check_phone(phone_numb)
                if auth_phone is False:
                    await bot.send_message(message.chat.id,
                                           'Номер не зарегестрирован в системе',
                                           reply_markup=keyboards.ReplyKeyboardRemove())
                    await bot.send_message(message.chat.id,
                                           'Пройти регистрацию:',
                                           reply_markup=keyboards.passregister())
                if auth_phone is True:
                    await bot.send_message(message.chat.id,
                                           'Введи свой пароль')
                    await mongo.set_user_state(str(message.chat.id), 'passauth')

            except Exception as e:
                erlog.exception(e)

        elif need_pass is False and user['state'] == 'nophone' and message.from_user.id == message.contact.user_id:
            try:
                phone_numb = ''.join(filter(lambda x: x.isdigit(), str(message.contact.phone_number)))
                await mongo.set_user_phone(str(message.chat.id), str(phone_numb), 'phone')
                auth_phone = await loyapi.auth_phone(phone_numb)

                if auth_phone == 'notreg':
                    await bot.send_message(message.chat.id,
                                           'Номер не зарегестрирован в системе',
                                           reply_markup=keyboards.ReplyKeyboardRemove())
                    await bot.send_message(message.chat.id,
                                           'пройти регистрацию:',
                                           reply_markup=keyboards.register())
                elif auth_phone[0] == 'ok':
                    await mongo.set_user_state(str(message.chat.id), {'auth': auth_phone[1]})
                    await bot.send_message(message.chat.id,
                                           'Отлично!\nВведите код из SMS сообщения)',
                                           reply_markup=keyboards.ReplyKeyboardRemove())
                elif auth_phone[0] == 'error':
                    await bot.send_message(message.chat.id, f'Произошла ошибка\n {auth_phone[1]}')

            except Exception as e:
                erlog.exception(e)

        elif 'phone' in user:
            await bot.send_message(message.chat.id, "Спасибо, но я уже запомнил твой номер)")
        else:
            await bot.send_message(message.chat.id, "Хммм.... подозрительно\nКажется это не твой номер...")

    except Exception as e:
        erlog.exception(e)


@dp.message_handler()
# TODO finish registration
async def auth(msg):
    try:
        user = await mongo.find_user(str(msg.chat.id))

# IF PASSregister in state

        if user and 'passregister' in user['state']:
            passreg = await loyapi.pass_registration(user['phone'], msg.text)
            if passreg:
                await mongo.set_user_state(str(msg.chat.id), {'passreg': str(passreg)})
                await bot.send_message(msg.chat.id, 'Введите код из SMS сообщения')
            if not passreg:
                await bot.send_message(msg.chat.id, 'Ошибка, что-то пошло не так...\n попробуйте ввести еще раз')

# IF PASSauth in state

        elif user and 'passauth' in user['state']:
            auth = await loyapi.pass_auth_phone(user['phone'], msg.text)
            if auth[0] == 'ok':
                token = auth[1]['data']['token']
                fields = {
                    'token': token,
                    'state': 'ready'
                }
                await mongo.write_field(str(msg.chat.id), **fields)
                bar = await loyapi.barcode(token)
                if bar:
                    barcode_url = bar['data']['bar_code']

                    await bot.send_message(msg.chat.id,
                                           f'Аутентификация успешна!'
                                           f'\nШтрих-код карты:\n\n{barcode_url}',
                                           reply_markup=keyboards.to_menu())
                else:
                    await bot.send_message(msg.chat.id,
                                           f'Аутентификация успешна!',
                                           reply_markup=keyboards.to_menu())

            elif auth[0] == 'err':
                err = auth[1]['data'][0]['message']

                await bot.send_message(msg.chat.id, f'Ошибка:\n{err}\nВведите пароль еще раз')

            else:
                buttons = {'Заново отправить смс': 'resend_sms'}
                await bot.send_message(msg.chat.id,
                                       'Что-то пошло не так...\nПоробуйте ввести еще раз',
                                       reply_markup=keyboards.add_kb(**buttons))

        elif user and 'addcard' in user['state']:
            adding = await loyapi.addcard(user['token'], msg.text)
            if adding:
                try:
                    profile_n_cards = await loyapi.profile(user['token'])
                    if profile_n_cards:
                        mobile = profile_n_cards[1]['mobile']
                        email = profile_n_cards[1]['email']
                        first_name = profile_n_cards[1]['first_name']
                        last_name = profile_n_cards[1]['last_name']
                        birth_day = profile_n_cards[1]['birth_day']

                        await bot.send_message(chat_id=msg.chat.id,
                                               text='_Карта привязана!_\n'
                                                    f'*Имя*: {first_name}\n'
                                                    f'*Фамилия*: {last_name}\n'
                                                    f'*Дата рождения*: {birth_day}\n'
                                                    f'*Номер телефона*: {mobile}\n'
                                                    f'*E-mail*: {email}\n'
                                                    f'*№ карты*:{profile_n_cards[2]}',
                                               reply_markup=keyboards.profile(),
                                               parse_mode='Markdown')
                    await mongo.set_user_state(str(msg.chat.id), 'ready')

                except Exception as e:
                    erlog.exception(e)

            else:
                await bot.send_message(msg.chat.id, 'Карта не найдена', reply_markup=keyboards.to_menu())



#    IF editable items in state

        elif user and user['state'] in ['first_name', 'last_name', 'email', 'birth_day']:
            token = user['token']
            update_data = {user['state']: msg.text}
            edit_prof = await loyapi.edit_profile(token, update_data)
            if edit_prof == 200:
                profile_n_cards = await loyapi.profile(user['token'])
                if profile_n_cards:
                    mobile = profile_n_cards[1]['mobile']
                    email = profile_n_cards[1]['email']
                    first_name = profile_n_cards[1]['first_name']
                    last_name = profile_n_cards[1]['last_name']
                    birth_day = profile_n_cards[1]['birth_day']

                    await bot.send_message(chat_id=msg.chat.id,
                                           text=f'*Имя*: {first_name}\n'
                                                f'*Фамилия*: {last_name}\n'
                                                f'*Дата рождения*: {birth_day}\n'
                                                f'*Номер телефона*: {mobile}\n'
                                                f'*E-mail*: {email}\n'
                                                f'*№ карты*:{profile_n_cards[2]}',
                                           reply_markup=keyboards.profile(),
                                           parse_mode='Markdown')
            if edit_prof == 422:
                await bot.send_message(str(msg.chat.id), 'Неверный формат даты\n'
                                                         'Пример: 1986-06-28',
                                       reply_markup=keyboards.cancel())

#  Если в статусе пользователя - словарь:

        elif user and isinstance(user['state'], dict):
            if msg.text.isdigit():
                if 'passreg' in user['state']:
                    sms_id = user['state']['passreg']
                    auth_phone = await loyapi.auth_registration_confirm(sms_id, str(msg.text))
                    if auth_phone:
                        token = auth_phone['data']['token']
                        fields = {
                            'token': token,
                            'state': 'ready'
                        }
                        await mongo.write_field(str(msg.chat.id), **fields)
                        bar = await loyapi.barcode(token)
                        if bar:
                            barcode_url = bar['data']['bar_code']

                            await bot.send_message(msg.chat.id,
                                                   f'Регистрация успешна!'
                                                   f'\nШтрих-код карты:\n\n{barcode_url}',
                                                   reply_markup=keyboards.to_menu())
                        else:
                            await bot.send_message(msg.chat.id,
                                                   f'Регистрация успешна!',
                                                   reply_markup=keyboards.to_menu())
                    else:
                        buttons = {'Заново отправить смс': 'resend_sms'}
                        await bot.send_message(msg.chat.id,
                                               'Что-то пошло не так...\nПоробуйте ввести еще раз',
                                               reply_markup=keyboards.add_kb(**buttons))

                if 'auth' in user['state']:
                    sms_id = user['state']['auth']
                    auth = await loyapi.auth_phone_confirm(sms_id, str(msg.text))
                    #print(auth)
                    if 'token' in auth['data']:
                        token = auth['data']['token']
                        fields = {
                            'token': token,
                            'state': 'ready'
                        }
                        await mongo.write_field(str(msg.chat.id), **fields)

                        bar = await loyapi.barcode(token)
                        if bar is not False:
                            barcode_url = bar['data']['bar_code']

                            await bot.send_message(msg.chat.id,
                                                   'Аутентификация успешна!\n'
                                                   f'Штрих-код карты:\n\n{barcode_url}',
                                                   reply_markup=keyboards.to_menu())
                        else:
                            await bot.send_message(msg.chat.id,
                                                   'Аутентификация успешна!\n',
                                                   reply_markup=keyboards.to_menu())
                    else:
                        buttons = {'Заново отправить смс': 'resend_sms'}
                        await bot.send_message(msg.chat.id,
                                               'Что-то пошло не так...\nПоробуйте ввести еще раз',
                                               reply_markup=keyboards.add_kb(**buttons))

                if 'reg' in user['state']:
                    auth_phone = await loyapi.auth_registration_confirm(user['state']['reg'], str(msg.text))
                    if 'token' in auth_phone['data']:
                        token = auth_phone['data']['token']
                        fields = {
                            'token': token,
                            'state': 'ready'
                        }
                        await mongo.write_field(str(msg.chat.id), **fields)

                        bar = await loyapi.barcode(token)
                        if bar:
                            barcode_url = bar['data']['bar_code']

                            await bot.send_message(msg.chat.id,
                                                   f'Регистрация успешна!'
                                                   f'\nШтрих-код карты:\n\n{barcode_url}',
                                                   reply_markup=keyboards.to_menu())
                        else:
                            await bot.send_message(msg.chat.id,
                                                   f'Регистрация успешна!',
                                                   reply_markup=keyboards.to_menu())

                    else:
                        buttons = {'Заново отправить смс': 'resend_sms'}
                        await bot.send_message(msg.chat.id,
                                               'Что-то пошло не так...\nПоробуйте ввести еще раз',
                                               reply_markup=keyboards.add_kb(**buttons))

            elif not msg.text.isdigit():
                await bot.send_message(msg.chat.id,
                                       'Пароль должен состоять из цифр!')

        elif user is not False and 'nophone' in user['state']:
            await bot.send_message(msg.chat.id,
                                   f'Вы не зарегестрированы в системе!\n'
                                   f'Для начала работы поделись номером телефона)',
                                   reply_markup=keyboards.phonenum())
    except Exception as e:
        erlog.exception(e)


@dp.callback_query_handler(text='passregister')
async def passregister(call):
    await bot.answer_callback_query(call.id)
    try:
        await bot.send_message(call.from_user.id,
                               'Придумай и отправь мне пароль:')
        await mongo.set_user_state(str(call.from_user.id), 'passregister')

    except Exception as e:
        erlog.exception(e)


@dp.callback_query_handler(text='register')
async def register(call):
    await bot.answer_callback_query(call.id)
    try:
        user = await mongo.find_user(str(call.from_user.id))
        reg = await loyapi.registration(user['phone'])

        if reg[0] == 'ok':
            sms_id = reg[1]
            await mongo.set_user_state(str(call.from_user.id), {'reg': sms_id})
            await bot.send_message(call.from_user.id, 'Введите код из SMS сообщения')

        else:
            msg = reg[1]['data']['message']
            await bot.send_message(call.from_user.id, str(msg))

    except Exception as e:
        erlog.exception(e)


@dp.callback_query_handler(text='to_menu')
async def to_menu(call):
    await bot.answer_callback_query(call.id)
    await bot.edit_message_text(chat_id=call.from_user.id,
                                message_id=call.message.message_id,
                                text=f'Приветствую, {call.from_user.first_name}!\nДобро пожаловать в ABM Loyalty!!!',
                                reply_markup=keyboards.menu())
    await mongo.set_user_state(str(call.from_user.id), 'ready')


@dp.callback_query_handler(text='barcode')
async def barcode(call):
    user = await mongo.find_user(str(call.from_user.id))
    await bot.answer_callback_query(call.id)
    try:
        bar = await loyapi.barcode(user['token'])
        if bar:
            barcode_url = bar['data']['bar_code']

            await bot.edit_message_text(chat_id=call.from_user.id,
                                        message_id=call.message.message_id,
                                        text=f'Ваш штрих-код:\n\n{barcode_url}',
                                        reply_markup=keyboards.to_menu())
        else:
            await bot.edit_message_text(chat_id=call.from_user.id,
                                        message_id=call.message.message_id,
                                        text=f'Невозможно получить информацию',
                                        reply_markup=keyboards.to_menu())
    except Exception as e:
        erlog.exception(e)


@dp.callback_query_handler(text='qrcode')
async def qrcode(call):
    user = await mongo.find_user(str(call.from_user.id))
    await bot.answer_callback_query(call.id)
    try:
        qr = await loyapi.qrcode(user['token'])
        if qr:
            qrcode_url = qr['data']['qr_code']

            await bot.edit_message_text(chat_id=call.from_user.id,
                                        message_id=call.message.message_id,
                                        text=f'Ваш QR-код:\n\n{qrcode_url}',
                                        reply_markup=keyboards.to_menu())
        else:
            await bot.edit_message_text(chat_id=call.from_user.id,
                                        message_id=call.message.message_id,
                                        text=f'Невозможно получить информацию',
                                        reply_markup=keyboards.to_menu())
    except Exception as e:
        erlog.exception(e)


@dp.callback_query_handler(text='bonbal')
async def balance(call):
    user = await mongo.find_user(str(call.from_user.id))
    await bot.answer_callback_query(call.id)
    try:
        bal = await loyapi.balance(user['token'])
        if bal[0] == 'ok':
            bonbal = bal[1]
            available = bal[2]

            await bot.edit_message_text(chat_id=call.from_user.id,
                                        message_id=call.message.message_id,
                                        text=f'Баланс: {bonbal}\n'
                                             f'Доступно: {available}',
                                        reply_markup=keyboards.to_menu())
        else:
            erlog.error(str(bal[1]))
            await bot.edit_message_text(chat_id=call.from_user.id,
                                        message_id=call.message.message_id,
                                        text=f'Невозможно получить информацию',
                                        reply_markup=keyboards.to_menu())
    except TypeError:
        await bot.edit_message_text(chat_id=call.from_user.id,
                                    message_id=call.message.message_id,
                                    text=f'Невозможно получить информацию',
                                    reply_markup=keyboards.to_menu())
    except Exception as e:
        erlog.exception(e)



@dp.callback_query_handler(text='actions')
async def balance(call):
    user = await mongo.find_user(str(call.from_user.id))
    await bot.answer_callback_query(call.id)
    actions = await loyapi.actions(user['token'])

    if actions:
        items = actions['data']['items']
        pagactions.init_user(call.from_user.id, len(items))
        company = items[0]['partner']['company']
        title = items[0]['title']
        content = items[0]['content']
        act_from = items[0]['act_from']
        act_to = items[0]['act_to']
        img_path = items[0]['img_path']

        #print(company, title, content, act_from, act_to, img_path)

        await bot.edit_message_text(chat_id=call.from_user.id,
                                    message_id=call.message.message_id,
                                    text=f'<b>{title}</b>\n'
                                         f'{content}\n\n'
                                         f'<b>{company}</b>\n'
                                         f'<b>Период действия:</b> <i>c {act_from} по {act_to}</i>\n\n'
                                         f'{img_path}',
                                    reply_markup=keyboards.actions(),
                                    parse_mode='HTML')

    else:
        print("ERORORO ")

@dp.callback_query_handler(text='nextact')
async def next_news(call):
    user = await mongo.find_user(str(call.from_user.id))
    await bot.answer_callback_query(call.id)

    status = pagactions.move_forw(call.from_user.id)

    actions = await loyapi.actions(user['token'])

    if actions:
        items = actions['data']['items']

        company = items[status]['partner']['company']
        title = items[status]['title']
        content = items[status]['content']
        act_from = items[status]['act_from']
        act_to = items[status]['act_to']
        img_path = items[status]['img_path']

        #print(company, title, content, act_from, act_to, img_path)

        await bot.edit_message_text(chat_id=call.from_user.id,
                                    message_id=call.message.message_id,
                                    text=f'<b>{title}</b>\n'
                                         f'{content}\n\n'
                                         f'<b>{company}</b>\n'
                                         f'<b>Период действия:</b> <i>c {act_from} по {act_to}</i>\n\n'
                                         f'{img_path}',
                                    reply_markup=keyboards.actions(),
                                    parse_mode='HTML')

    else:
        print("ERORORO ")

@dp.callback_query_handler(text='prevact')
async def next_news(call):
    user = await mongo.find_user(str(call.from_user.id))
    await bot.answer_callback_query(call.id)

    status = pagactions.move_back(call.from_user.id)

    actions = await loyapi.actions(user['token'])

    if actions:
        items = actions['data']['items']
        company = items[status]['partner']['company']
        title = items[status]['title']
        content = items[status]['content']
        act_from = items[status]['act_from']
        act_to = items[status]['act_to']
        img_path = items[status]['img_path']

        #print(company, title, content, act_from, act_to, img_path)

        await bot.edit_message_text(chat_id=call.from_user.id,
                                    message_id=call.message.message_id,
                                    text=f'<b>{title}</b>\n'
                                         f'{content}\n\n'
                                         f'<b>{company}</b>\n'
                                         f'<b>Период действия:</b> <i>c {act_from} по {act_to}</i>\n\n'
                                         f'{img_path}',
                                    reply_markup=keyboards.actions(),
                                    parse_mode='HTML')

    else:
        print("ERORORO ")


@dp.callback_query_handler(text='news')
async def balance(call):
    user = await mongo.find_user(str(call.from_user.id))
    await bot.answer_callback_query(call.id)
    news = await loyapi.news(user['token'])
    if news:
        news0 = news['data']['items'][0]
        pagnews.init_user(call.from_user.id, len(news['data']['items']))

        name = news0['name']
        desc = news0['description_short']
        link = news0['img_path']

        await bot.edit_message_text(chat_id=call.from_user.id,
                                    message_id=call.message.message_id,
                                    text=f'<b>{name}</b>\n'
                                         f'{desc}\n\n'
                                         f'{link}',
                                    reply_markup=keyboards.news(),
                                    parse_mode='HTML')

    else:
        print('none')


@dp.callback_query_handler(text='nextnews')
async def next_news(call):
    user = await mongo.find_user(str(call.from_user.id))
    await bot.answer_callback_query(call.id)

    status = pagnews.move_forw(call.from_user.id)

    news = await loyapi.news(user['token'])
    if news:
        new = news['data']['items'][status]
        #pagnews.init_user(call.from_user.id, len(news['data']['items']))

        name = new['name']
        desc = new['description_short']
        link = new['img_path']

        await bot.edit_message_text(chat_id=call.from_user.id,
                                    message_id=call.message.message_id,
                                    text=f'<b>{name}</b>\n'
                                         f'{desc}\n\n'
                                         f'{link}',
                                    reply_markup=keyboards.news(),
                                    parse_mode='HTML')


@dp.callback_query_handler(text='prevnews')
async def next_news(call):
    user = await mongo.find_user(str(call.from_user.id))
    await bot.answer_callback_query(call.id)

    status = pagnews.move_back(call.from_user.id)


    news = await loyapi.news(user['token'])
    if news:
        new = news['data']['items'][status]
        name = new['name']
        desc = new['description_short']
        link = new['img_path']

        await bot.edit_message_text(chat_id=call.from_user.id,
                                    message_id=call.message.message_id,
                                    text=f'<b>{name}</b>\n'
                                         f'{desc}\n\n'
                                         f'{link}',
                                    reply_markup=keyboards.news(),
                                    parse_mode='HTML')


@dp.callback_query_handler(text='profile')
async def profile(call):
    await bot.answer_callback_query(call.id)
    user = await mongo.find_user(str(call.from_user.id))
    try:
        profile_n_cards = await loyapi.profile(user['token'])
        if profile_n_cards:
            mobile = profile_n_cards[1]['mobile']
            email = profile_n_cards[1]['email']
            first_name = profile_n_cards[1]['first_name']
            last_name = profile_n_cards[1]['last_name']
            birth_day = profile_n_cards[1]['birth_day']

            await bot.edit_message_text(chat_id=call.from_user.id,
                                        message_id=call.message.message_id,
                                        text=f'*Имя*: {first_name}\n'
                                             f'*Фамилия*: {last_name}\n'
                                             f'*Дата рождения*: {birth_day}\n'
                                             f'*Номер телефона*: {mobile}\n'
                                             f'*E-mail*: {email}\n'
                                             f'*№ карты*:{profile_n_cards[2]}',
                                        reply_markup=keyboards.profile(),
                                        parse_mode='Markdown')

    except Exception as e:
        erlog.exception(e)


@dp.callback_query_handler(text='edit_profile')
async def edit_profile(call):
    await bot.answer_callback_query(call.id)
    user = await mongo.find_user(str(call.from_user.id))
    try:
        profile_cards = await loyapi.profile(user['token'])
        if profile_cards and profile_cards[0] == 'ok':
            bday = profile_cards[1]['birth_day']
            if not bday:
                buttons = {
                    'Имя': 'first_name',
                    'Фамилия': 'last_name',
                    'Дата рождения': 'birth_day',
                    'E-mail': 'email',
                    'Меню': 'to_menu'
                }
                await bot.edit_message_text(chat_id=call.from_user.id,
                                            message_id=call.message.message_id,
                                            text= 'Выбери параметр для редактирования:\n',
                                            reply_markup=keyboards.add_kb(**buttons),
                                            parse_mode='Markdown'
                                            )
            else:
                buttons = {
                    'Имя': 'first_name',
                    'Фамилия': 'last_name',
                    'E-mail': 'email',
                    'Меню': 'to_menu'
                }
                await bot.edit_message_text(chat_id=call.from_user.id,
                                            message_id=call.message.message_id,
                                            text='Выбери параметр для редактирования:',
                                            reply_markup=keyboards.add_kb(**buttons),
                                            parse_mode='Markdown'
                                            )

    except Exception as e:
        erlog.exception(e)


@dp.callback_query_handler(text='cancel')
async def cancel(call):
    await bot.answer_callback_query(call.id)
    await mongo.set_user_state(str(call.from_user.id), 'ready')
    await bot.edit_message_text(chat_id=call.from_user.id,
                                message_id=call.message.message_id,
                                text=f'Приветствую, {call.from_user.first_name}!\nДобро пожаловать в ABM Loyalty!!!',
                                reply_markup=keyboards.menu())


@dp.callback_query_handler(lambda callback: callback.data in ['first_name', 'last_name', 'email', 'birth_day'])
async def editing_profile(call):
    editwhat = {
        'first_name': 'Имя',
        'last_name': 'Фамилию',
        'email': 'E-mail',
        'birth_day': 'Дату рождения'}

    try:
        if call.data == 'birth_day':
            await bot.answer_callback_query(call.id,
                                            'Внимание!\n\nДату рождения можно задать только один раз',
                                            show_alert=True)
            await mongo.set_user_state(str(call.from_user.id), call.data)
            await bot.edit_message_text(chat_id=call.from_user.id,
                                        message_id=call.message.message_id,
                                        text='Редактируем: *Дату рождения*\n'
                                             'Просто напиши мне новый вариант\n'
                                             '_Формат даты_: *1985-05-25*\n'
                                             '*Внимание!*\n_Дату рождения можно задать только один раз_',
                                        reply_markup=keyboards.cancel(),
                                        parse_mode='Markdown')
        else:
            await bot.answer_callback_query(call.id)
            await mongo.set_user_state(str(call.from_user.id), call.data)
            await bot.edit_message_text(chat_id=call.from_user.id,
                                        message_id=call.message.message_id,
                                        text=f'Редактируем: *{editwhat[call.data]}*\n'
                                             'Просто напиши мне новый вариант',
                                        reply_markup=keyboards.cancel(),
                                        parse_mode='Markdown')

    except Exception as e:
        erlog.exception(e)


@dp.callback_query_handler(text='purchase')
async def purchase(call):
    user = await mongo.find_user(str(call.from_user.id))
    token = user['token']

    purchases = await loyapi.list_purchases(token)
    if len(purchases['data']['purchases']) > 0:

        pag_init = pag.init_user(call.from_user.id, len(purchases['data']['purchases']))

        if pag_init['state'] == pag_init['max']:
            pag_kb = 0
        else:
            pag_kb = 1

        if purchases:
            purch = purchases['data']['purchases']
            buttons = {}

            def values(num, p):
                numb = num[0]
                shop = p['transactions'][0]['shop']
                paysum = p['transactions'][0]['pay_sum']
                paybonus = p['transactions'][0]['pay_bonus']
                paydate = p['transactions'][0]['formatted_pay_date']
                paytime = p['transactions'][0]['formatted_pay_time']
                trans_item = p['transactions'][0]['processing_transaction_id']
                # print(type(trans_item))
                buttons[f'{str(numb)}'] = f'id{str(trans_item)}'

                return f'{numb}\u20E3\n \U0001F4CD{shop}\n' \
                       f'\U0001F4B8 {paysum}грн ({paybonus}BON)\n' \
                       f'\U0001F4C5{paydate}({paytime})\n\n'

            s = ''.join(map(values, enumerate(purch, 1), purch[:3]))

            try:
                await bot.edit_message_text(chat_id=call.from_user.id,
                                            message_id=call.message.message_id,
                                            text=s,
                                            reply_markup=keyboards.purchase_kb(pag_kb, **buttons))
                await bot.answer_callback_query(call.id)

            except Exception as e:
                await bot.answer_callback_query(call.id, f'{e}')
                print(e)

            import sys
            print(sys.getsizeof(s))
    else:
        await bot.answer_callback_query(call.id, 'Нет последних покупок.')


@dp.callback_query_handler(text='forward')
async def next_items(call):
    await bot.answer_callback_query(call.id)

    user = await mongo.find_user(str(call.from_user.id))
    token = user['token']
    purchases = await loyapi.list_purchases(token)
    purch = purchases['data']['purchases']

    pag.move_forw(call.from_user.id)
    pag_state = pag.get_state(call.from_user.id)

    print(pag_state)

    end = pag_state['state'] * 3
    start = end - 3

    buttons = {}

    def values(num, p):
        numb = num[0]
        shop = p['transactions'][0]['shop']
        paysum = p['transactions'][0]['pay_sum']
        paybonus = p['transactions'][0]['pay_bonus']
        paydate = p['transactions'][0]['formatted_pay_date']
        paytime = p['transactions'][0]['formatted_pay_time']
        trans_item = p['transactions'][0]['processing_transaction_id']
        # print(type(trans_item))
        buttons[f'{str(numb)}'] = f'id{str(trans_item)}'

        return f'{numb}\u20E3\n \U0001F4CD{shop}\n' \
               f'\U0001F4B8 {paysum}грн ({paybonus}BON)\n' \
               f'\U0001F4C5{paydate}({paytime})\n\n'

    s = ''.join(map(values, enumerate(purch, 1), purch[start: end]))

    pag_kb = 0
    if pag_state['state'] == pag_state['max']:
        pag_kb = 2
    elif pag_state['state'] < pag_state['max']:
        pag_kb = 3

    try:
        await bot.edit_message_text(chat_id=call.from_user.id,
                                    message_id=call.message.message_id,
                                    text=s,
                                    reply_markup=keyboards.purchase_kb(pag_kb, **buttons))
        await bot.answer_callback_query(call.id)

    except Exception as e:
        await bot.answer_callback_query(call.id, f'{e}')
        print(e)


@dp.callback_query_handler(text='back')
async def next_items(call):
    await bot.answer_callback_query(call.id)

    user = await mongo.find_user(str(call.from_user.id))
    token = user['token']
    purchases = await loyapi.list_purchases(token)
    purch = purchases['data']['purchases']

    pag.move_back(call.from_user.id)
    pag_state = pag.get_state(call.from_user.id)
    #print(pag_state)

    end = pag_state['state'] * 3
    start = end - 3

    buttons = {}

    def values(num, p):
        numb = num[0]
        shop = p['transactions'][0]['shop']
        paysum = p['transactions'][0]['pay_sum']
        paybonus = p['transactions'][0]['pay_bonus']
        paydate = p['transactions'][0]['formatted_pay_date']
        paytime = p['transactions'][0]['formatted_pay_time']
        trans_item = p['transactions'][0]['processing_transaction_id']
        # print(type(trans_item))
        buttons[f'{str(numb)}'] = f'id{str(trans_item)}'

        return f'{numb}\u20E3\n \U0001F4CD{shop}\n' \
               f'\U0001F4B8 {paysum}грн ({paybonus}BON)\n' \
               f'\U0001F4C5{paydate}({paytime})\n\n'

    s = ''.join(map(values, enumerate(purch, 1), purch[start: end]))

    pag_kb = 0
    if pag_state['state'] == 1:
        pag_kb = 1
    elif pag_state['state'] < pag_state['max']:
        pag_kb = 3

    await bot.edit_message_text(chat_id=call.from_user.id,
                                message_id=call.message.message_id,
                                text=s,
                                reply_markup=keyboards.purchase_kb(pag_kb, **buttons))


@dp.callback_query_handler(lambda call: call.data.startswith('id'))
async def get_item(call):
    await bot.answer_callback_query(call.id, str(call.data))


    user = await mongo.find_user(str(call.from_user.id))
    token = user['token']
    purchases = await loyapi.list_purchases(token)
    purch = purchases['data']['purchases']
    purch_id = call.data[2:]

    for item in purch:
        if item['transactions'][0]['processing_transaction_id'] == int(purch_id):
            pos = item['transactions'][0]['check_positions']

            def values(p):
                prod_name = p['product_name']
                price = p['product_price']
                amount = p['product_amount']
                prod_sum = p['product_sum']

                return f'\U0001F680Товар: {prod_name}\n' \
                       f'  Цена: {price}грн\n' \
                       f'  Кол-во: {amount}\n' \
                       f'  Сумма: {prod_sum}грн\n\n'


            s = ''.join(map(values, pos))
            summ = item['transactions'][0]['pay_sum']
            bon = item['transactions'][0]['pay_bonus']
            await bot.edit_message_text(chat_id=call.from_user.id,
                                        message_id=call.message.message_id,
                                        text=f'{s}Итого: {summ}грн({bon}BON)',
                                        reply_markup=keyboards.to_menu())


@dp.callback_query_handler(text='addcard')
async def addcard(call):
    await bot.answer_callback_query(call.id)
    user = await mongo.find_user(str(call.from_user.id))
    await mongo.set_user_state(str(call.from_user.id), 'addcard')
    await bot.edit_message_text(chat_id=call.from_user.id,
                                message_id=call.message.message_id,
                                text='Введите номер карты',
                                reply_markup=keyboards.to_menu())


#                           If user push cross button (pagination)
@dp.callback_query_handler(text='crossmark')
async def crossmark(call):
    await bot.answer_callback_query(call.id)



async def on_shutdown(app):
    """
    Graceful shutdown. This method is recommended by aiohttp docs.
    """
    await bot.close()

if __name__ == '__main__':
    # Get instance of :class:`aiohttp.web.Application` with configured router.
    app = get_new_configured_app(dispatcher=dp, path=WEBHOOK_URL_PATH)

    # Setup event handlers.
    app.on_shutdown.append(on_shutdown)

    # Start web-application.
    web.run_app(app, host=WEBAPP_HOST, port=WEBAPP_PORT)
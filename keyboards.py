import aiogram

from aiogram.types.inline_keyboard import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram.types.reply_keyboard import ReplyKeyboardMarkup, KeyboardButton, ReplyKeyboardRemove

#                                REPLY kb


def phonenum():
    kb = ReplyKeyboardMarkup(row_width=1, resize_keyboard=True, one_time_keyboard=True)
    phonebutt = KeyboardButton('Отправить номер телефона', request_contact=True)
    kb.add(phonebutt)
    return kb


#                             INLINE kb

def add_kb(**kwargs):
    kb = InlineKeyboardMarkup()
    for key, val in kwargs.items():
        button = InlineKeyboardButton(f'{key}', callback_data=f'{val}')
        kb.add(button)
    return kb


def purchase_kb(number, **kwargs):
    kb = InlineKeyboardMarkup(row_width=5)

    if number == 0:
        l = []
        for key, val in kwargs.items():
            button = InlineKeyboardButton(f'{key}\u20E3', callback_data=f'{val}')
            l.append(button)
        kb.add(*l)
        butt3 = InlineKeyboardButton('Меню', callback_data='to_menu')
        kb.add(butt3)
        return kb

    elif number == 1:
        l = []
        for key, val in kwargs.items():
            button = InlineKeyboardButton(f'{key}\u20E3', callback_data=f'{val}')
            l.append(button)
        kb.add(*l)
        butt2 = InlineKeyboardButton('\U00002716', callback_data='crossmark')
        butt1 = InlineKeyboardButton('\U000023E9', callback_data='forward')
        butt3 = InlineKeyboardButton('Меню', callback_data='to_menu')
        kb.add(butt2, butt3, butt1)
        return kb

    elif number == 2:
        l = []
        for key, val in kwargs.items():
            button = InlineKeyboardButton(f'{key}\u20E3', callback_data=f'{val}')
            l.append(button)

        kb.add(*l)
        butt2 = InlineKeyboardButton('\U000023EA', callback_data='back')
        butt3 = InlineKeyboardButton('Меню', callback_data='to_menu')
        butt1 = InlineKeyboardButton('\U00002716', callback_data='crossmark')
        kb.add(butt2, butt3, butt1)
        return kb

    else:
        l = []
        for key, val in kwargs.items():
            button = InlineKeyboardButton(f'{key}\u20E3', callback_data=f'{val}')
            l.append(button)
        kb.add(*l)
        butt1 = InlineKeyboardButton('\U000023E9', callback_data='forward')
        butt2 = InlineKeyboardButton('\U000023EA', callback_data='back')
        butt3 = InlineKeyboardButton('Меню', callback_data='to_menu')
        kb.add(butt2, butt3, butt1)
        return kb





def menu():
    kb = InlineKeyboardMarkup()
    button = InlineKeyboardButton('Мой баланс', callback_data='bonbal')
    button2 = InlineKeyboardButton('Акции!', callback_data='actions')
    button3 = InlineKeyboardButton('Новости', callback_data='news')
    button4 = InlineKeyboardButton('Мой профиль', callback_data='profile')
    button5 = InlineKeyboardButton('Мои покупки', callback_data='purchase')
    button6 = InlineKeyboardButton('Штрих-код', callback_data='barcode')
    button7 = InlineKeyboardButton('QR-код', callback_data='qrcode')
    kb.add(button6, button, button7)
    kb.add(button5)
    kb.add(button2, button4, button3)
    return kb


def register():
    kb = InlineKeyboardMarkup()
    button = InlineKeyboardButton('Регистрация', callback_data='register')
    kb.add(button)
    return kb


def passregister():
    kb = InlineKeyboardMarkup()
    button = InlineKeyboardButton('Регистрация', callback_data='passregister')
    kb.add(button)
    return kb


def to_menu():
    kb = InlineKeyboardMarkup()
    button = InlineKeyboardButton('Меню', callback_data='to_menu')
    kb.add(button)
    return kb

def cancel():
    kb = InlineKeyboardMarkup()
    button = InlineKeyboardButton('Отмена', callback_data='cancel')
    kb.add(button)
    return kb

def profile():
    keyboard = InlineKeyboardMarkup()
    button1 = InlineKeyboardButton('Меню', callback_data='to_menu')
    button2 = InlineKeyboardButton('Редактировать контактные данные', callback_data='edit_profile')
    button3 = InlineKeyboardButton('Добавить карту', callback_data='addcard')
    keyboard.add(button2)
    keyboard.add(button3)
    keyboard.add(button1)
    return keyboard


def news():
    kb = InlineKeyboardMarkup()
    butt1 = InlineKeyboardButton('\U000023E9', callback_data='nextnews')
    butt2 = InlineKeyboardButton('\U000023EA', callback_data='prevnews')
    butt3 = InlineKeyboardButton('Меню', callback_data='to_menu')
    kb.add(butt2, butt3, butt1)
    return kb

def actions():
    kb = InlineKeyboardMarkup()
    butt1 = InlineKeyboardButton('\U000023E9', callback_data='nextact')
    butt2 = InlineKeyboardButton('\U000023EA', callback_data='prevact')
    butt3 = InlineKeyboardButton('Меню', callback_data='to_menu')
    kb.add(butt2, butt3, butt1)
    return kb
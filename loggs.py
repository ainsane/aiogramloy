import logging


def setup_logger(log_name, log_file, level=logging.INFO):
    l = logging.getLogger(log_name)
    formatter = logging.Formatter('%(filename)s[LINE: %(lineno)d] - %(asctime)s - %(levelname)s - %(message)s')
    file_handler = logging.FileHandler(log_file)
    file_handler.setFormatter(formatter)

    l.setLevel(level)
    l.addHandler(file_handler)


setup_logger('errorLogger', 'ERROR.log')
setup_logger('infoLogger', 'INFO.log')

errorlog = logging.getLogger('errorLogger')
infolog = logging.getLogger('infoLogger')
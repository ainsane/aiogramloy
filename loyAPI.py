
import aiohttp
import loggs

erlog = loggs.errorlog
infolog = loggs.infolog

class LoyAPI:

    def __init__(self, loop):
        self.session = aiohttp.ClientSession(loop=loop)

    async def close(self):
        await self.session.close()

    async def auth_phone(self, phone_num):
        try:
            async with self.session.post('https://api.m-reward.com/v2.1/client/auth-phone',
                                         data={'phone': str(phone_num)}) as resp:
                if resp.status == 201:
                    sms = await resp.json()
                    sms_id = sms['data']['sms_id']
                    infolog.info(sms)
                    return ['ok', sms_id]

                elif resp.status == 422:
                    resp = await resp.json()
                    # доделать обработчик других вариантов которые спровоцированы 422
                    infolog.info(resp)
                    return 'notreg'

                else:
                    return ['error', resp.text]

        except Exception as e:
            erlog.exception(e)
            return ['error', e.args]

    async def auth_phone_confirm(self, sms_id, str_sms_pass):
        try:
            async with self.session.post('https://api.m-reward.com/v2.1/client/auth-phone-confirm',
                                         data={'code': f'{str_sms_pass}', 'sms_id': sms_id}) as resp:
                if resp.status == 201:
                    data = await resp.json()
                    infolog.info(data)
                    return data
                else:
                    erlog.error(str(resp.text))
                    return False

        except Exception as e:
            erlog.exception(e)

    async def pass_auth_phone(self, phone_num, passwd):
        try:
            async with self.session.post('https://api.m-reward.com/v2.1/client/auth-phone',
                                         data={'phone': str(phone_num), 'password': passwd}) as resp:
                data = await resp.json()
                if resp.status == 201:
                    return ['ok', data]
                elif resp.status == 422:
                    erlog.error(data)
                    return ['err', data]

        except Exception as e:
            erlog.exception(e)
            return ['error', e.args]

    async def registration(self, phone):
        try:
            async with self.session.post('https://api.m-reward.com/v2.1/client/registration',
                                         data={'phone': phone}) as resp:
                reg = await resp.json()
                infolog.info(reg)
                if resp.status == 201:
                    sms_id = reg['data']['sms_id']
                    return ['ok', sms_id]
                elif resp.status == 422:
                    return ['error1', reg]
                else:
                    return ['error', reg]
        except Exception as e:
            erlog.exception(e)

    async def auth_registration_confirm(self, sms_id, str_sms_pass):
        try:
            async with self.session.post('https://api.m-reward.com/v2.1/client/registration-confirm',
                                         data={'code': f'{str_sms_pass}', 'sms_id': sms_id}) as res:
                data = await res.json()
                infolog.info(data)
                if res.status == 201:
                    return data
                else:
                    erlog.error(data)
                    return None

        except Exception as e:
            erlog.exception(e)

    async def pass_registration(self, phone, passwd):
        try:
            async with self.session.post('https://api.m-reward.com/v2.1/client/registration',
                                         data={'phone': phone, 'password': passwd}) as resp:
                reg = await resp.json()
                infolog.info(reg)
                if resp.status == 201:
                    sms_id = reg['data']['sms_id']
                    return sms_id
                else:
                    erlog.error(reg)
                    return None
        except Exception as e:
            erlog.exception(e)

    async def check_phone(self, phone):
        try:
            async with self.session.post('https://api.m-reward.com/v2/client/check-phone',
                                         data={'phone': phone}) as res:
                data = await res.json()
                if data['data']['is_exist'] is True:
                    return True
                else:
                    return False
        except Exception as e:
            erlog.exception(e)

    async def ifneedpass(self):
        try:
            async with self.session.get('https://api.m-reward.com/v2.1/client/need-password') as res:
                data = await res.json()
                print(data)
                if data['data']['need_password'] is True:
                    return True
                else:
                    return False
        except Exception as e:
            erlog.exception(e)

    async def barcode(self, token):
        try:
            auth = aiohttp.BasicAuth(login=token)
            async with self.session.get('https://api.m-reward.com/v2/client/bar-code', auth=auth) as resp:
                data = await resp.json()
                infolog.info(data)
                if resp.status == 200:
                    return data
                else:
                    erlog.error(data)
                    return None
        except Exception as e:
            erlog.exception(e)

    async def qrcode(self, token):
        try:
            auth = aiohttp.BasicAuth(login=token)
            async with self.session.get('https://api.m-reward.com/v2/client/qr-code', auth=auth) as resp:
                data = await resp.json()
                infolog.info(data)
                if resp.status == 200:
                    return data
                else:
                    erlog.error(data)
                    return False
        except Exception as e:
            erlog.exception(e)

    async def balance(self, token):
        try:
            auth = aiohttp.BasicAuth(login=token)
            async with self.session.get('https://api.m-reward.com/v2/client/account/BON/balance', auth=auth)as resp:
                data = await resp.json()
                infolog.info(data)
                if resp.status == 200:
                    bal = data['data']['balance']
                    available = data['data']['balance']
                    return ['ok', bal, available]
                else:
                    return ['not_ok', data]
        except Exception as e:
            erlog.exception(e)

    async def profile(self, token):
        try:
            auth = aiohttp.BasicAuth(login=token)

            async with self.session.get('https://api.m-reward.com/v2/client/profile', auth=auth) as profile:

                async with self.session.get('https://api.m-reward.com/v2/client/card/card-info', auth=auth) as card:

                    if profile.status == 200 and card.status == 200:

                        prof_json = await profile.json()
                        card_json = await card.json()

                        cards = ''
                        for c in card_json['data']['cards']:
                            if c['number'].startswith('200'):
                                cards += f'\n{c["number"]} (virtual)'
                            else:
                                cards += f'\n{c["number"]}'

                        return ['ok', prof_json['data'], cards]
                    else:
                        erlog.error(await profile.text())
                        erlog.error(await card.text())
                        return None

        except Exception as e:
            erlog.exception(e)

    async def edit_profile(self, token, edit_data):
        try:
            auth = aiohttp.BasicAuth(login=token)
            async with self.session.put('https://api.m-reward.com/v2/client/profile',
                                        data=edit_data, auth=auth) as resp:
                print(await resp.json())
                if resp.status == 200:
                    return 200
                elif resp.status == 422:
                    return 422
                else:
                    erlog.error(await resp.json())
                    return None
        except Exception as e:
            erlog.exception(e)

    async def list_purchases(self, token):
        try:
            auth = aiohttp.BasicAuth(login=token)
            async with self.session.get('https://api.m-reward.com/v2/client/purchase-history',
                                        auth=auth) as resp:
                resp_json = await resp.json()
                if resp.status == 200:
                    return resp_json
                else:
                    erlog.error(resp_json)
                    return None
        except Exception as e:
            erlog.exception(e)

    async def news(self, token):
        try:
            auth = aiohttp.BasicAuth(login=token)
            async with self.session.get('https://api.m-reward.com/v2/client/partner/news-all',
                                        auth=auth) as resp:
                resp_json = await resp.json()
                if resp.status == 200:
                    return resp_json
                else:
                    erlog.error(resp_json)
                    return None
        except Exception as e:
            erlog.exception(e)

    async def actions(self, token):
        try:
            auth = aiohttp.BasicAuth(login=token)
            async with self.session.get('https://api.m-reward.com/v2/client/partner/actions',
                                        auth=auth) as resp:
                resp_json = await resp.json()
                if resp.status == 200:
                    return resp_json
                else:
                    erlog.error(resp_json)
                    return None
        except Exception as e:
            erlog.exception(e)

    async def addcard(self, token, cardnum):
        try:
            auth = aiohttp.BasicAuth(login=token)
            async with self.session.post('https://api.m-reward.com/v2/client/card/set-card',
                                         data={'number': cardnum},
                                         auth=auth) as resp:

                addcard = await resp.json()

                if resp.status == 201:
                    return addcard
                else:
                    #data = addcard["data"][0]['message']
                    return None
        except Exception as e:
            erlog.exception(e)
import motor.motor_asyncio
import loggs
#from asyncio import get_event_loop

erlog = loggs.errorlog

class MongoWorker:
    def __init__(self):
        self.client = motor.motor_asyncio.AsyncIOMotorClient('178.62.193.128', 27017)
        self.db = self.client.testDB
        self.coll = self.db.users

    async def write_user(self, str_chat_id, name, state):
        try:
            data = {
                '_id': str_chat_id,
                'state': state,
                'name': name
            }
            await self.coll.insert_one(data)
            return True

        except Exception as e:
            erlog.exception(e)
            return False

    async def find_user(self, str_chat_id):
        try:
            data = await self.coll.find_one({'_id': str_chat_id})
            if data is None:
                return None
            else:
                return data
        except Exception as e:
            erlog.exception(e)

    async def set_user_phone(self, str_chat_id, phone, state):
        try:
            data = {'phone': phone,
                    'state': state}
            await self.coll.update_one({'_id': str_chat_id}, {'$set': data})
            return True
        except Exception as e:
            erlog.exception(e)
            return False

    async def set_user_state(self, str_chat_id, state):
        try:
            upd = await self.coll.update_one({'_id': str_chat_id}, {'$set': {'state': state}})

            return True

        except Exception as e:
            erlog.exception(e)
            return False

    async def write_field(self, str_sender_id, **kwargs):
        d = {}
        for key, val in kwargs.items():
            d[key] = val
        try:
            print(d)
            update = await self.coll.update_one({'_id': str_sender_id}, {'$set': d})
            if update.modified_count == 1:
                return True
            else:
                return False
        except Exception as e:
            erlog.exception(e)



#loop = get_event_loop()
#cls = MongoWorker()
#mthd = cls.write_user('1122', 'WTF', 'phone')
#loop.run_until_complete(mthd)

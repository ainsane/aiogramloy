import math


class Pagination:

    def __init__(self):
        self.state = {}
        self.max = {}

    def init_user(self, uid, lenitems):
        self.state[uid] = 1
        self.max[uid] = math.ceil(lenitems / 3)
        return {'state': self.state[uid], 'max': self.max[uid]}

    def move_forw(self, uid):
        if self.max[uid] == 1:
            return
        if self.state[uid] < self.max[uid]:
            self.state[uid] += 1

    def move_back(self, uid):
        if self.state[uid] <= self.max[uid]:
            self.state[uid] -= 1

    def get_state(self, uid):
        state = self.state[uid]
        mit = self.max[uid]
        return {'state': state, 'max': mit}


class PaginationNewsActions:
    def __init__(self):
        self.state = {}
        self.max = {}

    def init_user(self, uid, lenitems):
        self.state[uid] = 0
        self.max[uid] = lenitems

    def move_forw(self, uid):
        if self.state[uid] >= self.max[uid] - 1:
            self.state[uid] = 0
        else:
            self.state[uid] += 1
        return self.state[uid]

    def move_back(self, uid):
        if self.state[uid] == -self.max[uid]:
            self.state[uid] = -1
        else:
            self.state[uid] -= 1
        return self.state[uid]

    def get_state(self, uid):
        return self.state[uid]